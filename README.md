# EPSI MSPR BIG DATA


## RUN DOCKER

```bash
docker-compose up
```

## INSTALL DEPENDENCIES

```bash
npm install
```

## FORMAT DATA

```bash
node_modules/csvtojson/bin/csvtojson dataset/data.csv > dataset/data.json
```

## RUN SCRIPT

```bash
npm run start
```

## RESET DATA

```bash
npm run reset
```

