const datas = require('./dataset/data.json');
const mapping = require('./dataset/mapping.json');

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ 
	node: 'http://localhost:9200',
    log: "trace",
    requestTimeout : 600000
})
const _ = require('lodash');

const indexName = "water";

console.log(`File length : ${datas.length}`)
console.log(`Index name : ${indexName}`)

let formatedDatas = [];

datas.forEach((curr) => {
	formatedDatas.push({ "index" : { "_index" : indexName, "_type" : "_doc", "_id" : curr.id } });
	curr.localisation = `${curr.latitude},${curr.longitude}`
	_.omit(curr, ['longitude', 'latitude']);
	curr.public_meeting = curr.public_meeting.toLowerCase() || "false"
	curr.permit = curr.permit.toLowerCase() || "false"

	formatedDatas.push(curr)
})


let index = new Promise((resolve, reject) => {

	console.log('CREATING INDEX');

	client.indices.create({
	    index: indexName,
	    body: {
	    	"mappings": {
	    		"_doc": {
	    			properties: mapping
	    		}
	    	}
	    }
	}, (err,resp, status) => {
	    if (err) {
    	  console.log('Error')
	      console.error(err, status);
	      console.log(err.body)
	      reject()
	    }
	    else {
	        console.log('Successfully Created Index', status, resp);
	        resolve()
	    }
	})
})


index.then(() => {
	console.log('SENDING DATAS');
	client.bulk({
	  body: formatedDatas
	}, function (err, resp) {
	    if(err) {
	    	console.log('Error')
	    	console.log(err)
	    } else {
	    	console.log(resp.body.items[0].index)
	    }
	});
})



